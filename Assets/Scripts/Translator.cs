﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translator : MonoBehaviour
{
    public int Speed;
    public Vector3 Vector3;
    public bool CanMove;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CanMove == true)
        {
            transform.Translate(Vector3 * Speed * Time.deltaTime);
        }     
    }
}
