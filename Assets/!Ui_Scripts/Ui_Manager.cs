﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ui_Manager : MonoBehaviour
{
    [Header("CoinDetails")]
    [Space(10)]
    public int Coins;
    public Text coin_Text;

    [Header("Panels_Detail")]
    [Space(10)]
    public GameObject Splash_Screen;
    public GameObject Bike_Selection;
    
    //Awake is Called Before Start
    void Awake()
    {
        Bike_Selection.SetActive(true);
        StartCoroutine("SplashFalse");
    }
    IEnumerator SplashFalse()
    {
        yield return new WaitForSeconds(5f);
        Splash_Screen.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        Coins=PlayerPrefs.GetInt("Coin", 0);
        coin_Text.text = Coins.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        Coins = PlayerPrefs.GetInt("Coin", 0);
        coin_Text.text = Coins.ToString();
    }
    public void DeleteCoin(int Price)
    {
        int Amount = PlayerPrefs.GetInt("Coin", 0);
        Amount -= Price;
        PlayerPrefs.SetInt("Coin", Amount);
    }
    public void CurrentAmount()
    {
        int Amount = PlayerPrefs.GetInt("Coin", 0);
        coin_Text.text = Amount.ToString();
    }
    public void MoreGames()
    {
        Application.OpenURL("https://play.google.com/store/apps/dev?id=6268191569591060634");
    }
}
