﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BikeInfo
{
    public int Price;
    public float Power;
    public float Break;
    public float Speed;
    public float Handling;
    public GameObject Bike;
   
}
public class Bike_Selection : MonoBehaviour
{
    [Header("BikesData")]
    [Space]
    public BikeInfo[] BikeData;
    public int current_Bike;

    [Header("UI_Refernce")]
    [Space]
    public Text Bike_Price;
    public Image Power_Img;
    public Image Break_Img;
    public Image Speed_Img;
    public Image Handling_Img;

    [Header("Play&Buy")]
    [Space]
    public GameObject Play_Button;
    public GameObject Buy_Button;

    [Header("Ui_Manager")]
    [Space]
    public GameObject Ui_Controller;

    // Start is called before the first frame update
    void Start()
    {

        //Take the Reference of Ui_Manager
        Ui_Controller = GameObject.Find("Ui_Controller");

        //Bike at 0 index That Is  Currently Unlocked
        PlayerPrefs.SetInt("Bike" + 0, 1);
        BikeStatus();


        //Bike at 0 Related Data
        foreach(BikeInfo x in BikeData)
        {
            x.Bike.SetActive(false);
        }
        BikeData[current_Bike].Bike.SetActive(true);
        Power_Img.fillAmount = BikeData[current_Bike].Power;
        Break_Img.fillAmount = BikeData[current_Bike].Break;
        Speed_Img.fillAmount = BikeData[current_Bike].Speed;
        Handling_Img.fillAmount = BikeData[current_Bike].Handling;
        Bike_Price.text ="Price:"+BikeData[current_Bike].Price.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnClickNext()
    {
        //Current Bike Related Data

        if (current_Bike<BikeData.Length-1)
        {
            current_Bike++;
        }
        foreach (BikeInfo x in BikeData)
        {
            x.Bike.SetActive(false);
        }
        BikeData[current_Bike].Bike.SetActive(true);
        Power_Img.fillAmount = BikeData[current_Bike].Power;
        Break_Img.fillAmount = BikeData[current_Bike].Break;
        Speed_Img.fillAmount = BikeData[current_Bike].Speed;
        Handling_Img.fillAmount = BikeData[current_Bike].Handling;
        Bike_Price.text = "Price:" + BikeData[current_Bike].Price.ToString();

        PlayerPrefs.SetInt("SelectedBike", current_Bike);

        BikeStatus();
    }
    public void OnClickPrevious()
    {
        //Current Bike Related Data

        if (current_Bike>0)
        {
            current_Bike--;
        }
        foreach (BikeInfo x in BikeData)
        {
            x.Bike.SetActive(false);
        }
        BikeData[current_Bike].Bike.SetActive(true);
        Power_Img.fillAmount = BikeData[current_Bike].Power;
        Break_Img.fillAmount = BikeData[current_Bike].Break;
        Speed_Img.fillAmount = BikeData[current_Bike].Speed;
        Handling_Img.fillAmount = BikeData[current_Bike].Handling;
        Bike_Price.text = "Price:" + BikeData[current_Bike].Price.ToString();

        PlayerPrefs.SetInt("SelectedBike", current_Bike);

        BikeStatus();
    }

    public void BikeStatus()
    {
        // To Check The Bike Whether it is Locked Or Unlocked

        int Locked = PlayerPrefs.GetInt("Bike"+current_Bike, 0);
        if(Locked==1)
        {
            Play_Button.SetActive(true);
            Buy_Button.SetActive(false);
        }
        if(Locked==0)
        {
            Play_Button.SetActive(false);
            Buy_Button.SetActive(true);
        }    
    }
    public void OnClickBuy()
    {
        // To Check if Amount is Grater or Equal tha Buy Selected Bike

        int AvaialbleAmount = PlayerPrefs.GetInt("Coin", 0);
        if(AvaialbleAmount>=BikeData[current_Bike].Price)
        {
            PlayerPrefs.SetInt("Bike", 1);
            BikeStatus();
            Ui_Controller.GetComponent<Ui_Manager>().DeleteCoin(BikeData[current_Bike].Price);
        }
    }
    public void OnClickExit()
    {
        Application.Quit();
    }
}
