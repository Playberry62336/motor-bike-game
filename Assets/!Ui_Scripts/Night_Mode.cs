﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Night_Mode : MonoBehaviour
{
    [Header("Level_Buttons")]
    [Space]
    public GameObject[] Levels_Buttons;
    public int Temp;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject x in Levels_Buttons)
        {
            x.GetComponent<Button>().interactable = false;
            x.transform.GetChild(0).transform.gameObject.SetActive(true);
            x.transform.GetChild(1).transform.gameObject.SetActive(false);
            x.transform.GetChild(2).transform.gameObject.SetActive(false);
        }
        Levels_Buttons[0].GetComponent<Button>().interactable = true;
        Levels_Buttons[0].transform.GetChild(0).transform.gameObject.SetActive(false);
        Levels_Buttons[0].transform.GetChild(1).transform.gameObject.SetActive(true);
        Levels_Buttons[0].transform.GetChild(2).transform.gameObject.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        // Update Ui Level Locked & Unlocked Ui..........

        foreach (GameObject x in Levels_Buttons)
        {
            x.GetComponent<Button>().interactable = false;
            x.transform.GetChild(0).transform.gameObject.SetActive(true);
            x.transform.GetChild(1).transform.gameObject.SetActive(false);
            x.transform.GetChild(2).transform.gameObject.SetActive(false);
        }

        int LevelUnlocked = PlayerPrefs.GetInt("NightValue", 0);
        for (int i = 0; i <= LevelUnlocked; i++)
        {
            Levels_Buttons[i].GetComponent<Button>().interactable = true;
            Levels_Buttons[i].transform.GetChild(0).transform.gameObject.SetActive(false);
            Levels_Buttons[i].transform.GetChild(1).transform.gameObject.SetActive(true);
            Levels_Buttons[i].transform.GetChild(2).transform.gameObject.SetActive(true);
        }

    }
    public void OnClickLevelButton(int i)
    {
        Temp = i;
        PlayerPrefs.SetInt("NightMode", i);
    }
    public void OnClickPlay()
    {
        PlayerPrefs.SetInt("NightMode", Temp);
        SceneManager.LoadScene(1);

    }
}
