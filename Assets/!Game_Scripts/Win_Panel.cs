﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win_Panel : MonoBehaviour
{
    public Text Coins;
    public Text Rewards;
    public Text Earned;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void Amount()
    {
        int Co = 500;
        Coins.text = Co.ToString();

        int Re = Random.Range(100, 301);
        Rewards.text = Re.ToString();

        int Ear = Co + Re;
        Earned.text = Ear.ToString();

        int Available=PlayerPrefs.GetInt("Coin", 0);
        Available = Available + Ear;
        PlayerPrefs.SetInt("Coin",Available);
        PlayerPrefs.Save();
    }
}
