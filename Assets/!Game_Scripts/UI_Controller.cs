﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Controller : MonoBehaviour
{
    [Header("Panels")]
    [Space(10)]
    public GameObject WinPanel;
    public GameObject Lose_Panel;
    public GameObject InGame_Panel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnClickPause()
    {
        Time.timeScale = 0f;
    }
    public void onClickResume()
    {
        Time.timeScale = 1f;
    }
    public void OnClickRestart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }
    public void OnClickMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }
}
