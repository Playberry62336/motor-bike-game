﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bike_Selector : MonoBehaviour
{
    [Header("Bikes_Data")]
    [Space(10)]
    public GameObject[] Bikes;
    public GameObject[] Biker_Pelvis;
    public int Current_Bike;

    [Header("Bike_Camera")]
    [Space(10)]
    public BikeCamera BC;

    [Header("Game_Controller")]
    [Space(10)]
    public Game_Controller GC;
    // AWake Is Called Before Start
    void Awake()
    {
        Current_Bike = PlayerPrefs.GetInt("SelectedBike", 0);
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject x in Bikes)
        {
            x.SetActive(false);
        }
        Bikes[Current_Bike].SetActive(true);
        BC.target = Bikes[Current_Bike].transform;
        BC.BikerMan = Biker_Pelvis[Current_Bike].transform;
        GC.BKC=Bikes[Current_Bike].GetComponent<BikeControl>();
    }
}
