﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Fail : MonoBehaviour
{
    [Header("Game_Controller")]
    [Space(10)]
    public Game_Controller GC;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            GC.UIC.Lose_Panel.SetActive(true);
            GC.UIC.InGame_Panel.SetActive(false);
        }
    }
}
