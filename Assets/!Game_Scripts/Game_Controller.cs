﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Game_Controller : MonoBehaviour
{
    [Header("Bike_Controller")]
    [Space(10)]
    public BikeControl BKC;

    [Header("Ui_Controller")]
    [Space(10)]
    public UI_Controller UIC;


    [Header("Modes")]
    [Space(10)]
    public Level_Selection[] Mode;
    public int Current_Level;
    public int Game_Mode;


    [Header("SkyBox")]
    [Space(10)]
    public Material DayTime;
    public Material NightTime;

    // Awake is Called Before Start
    void Awake()
    {
        Game_Mode = PlayerPrefs.GetInt("GameMode", 0);

        if (Game_Mode == 0)
        {
            Current_Level = PlayerPrefs.GetInt("DayMode", 0);
            RenderSettings.skybox = DayTime;
        }
        if (Game_Mode == 1)
        {
            RenderSettings.skybox = NightTime;
            Current_Level = PlayerPrefs.GetInt("NightMode", 0);
        }

        Mode[Game_Mode].Levels[Current_Level].SetActive(true);
    }

    public void OnClickNext()
    {
        if(Game_Mode==0)
        {
            int Level = PlayerPrefs.GetInt("DayMode", 0);
            Level = Level + 1;
            PlayerPrefs.SetInt("DayMode", Level);


            int DayTime = PlayerPrefs.GetInt("DayValue", 0);
            DayTime= DayTime + 1;
            PlayerPrefs.SetInt("DayValue", DayTime);
        }
        if (Game_Mode == 1)
        {
            int Level = PlayerPrefs.GetInt("NightMode", 0);
            Level = Level + 1;
            PlayerPrefs.SetInt("NightMode", Level);


            int NightTime = PlayerPrefs.GetInt("NightValue", 0);
            NightTime = NightTime + 1;
            PlayerPrefs.SetInt("NightValue", NightTime);
        }
        SceneManager.LoadScene(1);
    }
}
